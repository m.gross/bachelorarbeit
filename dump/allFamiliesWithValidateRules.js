/* Arten von Regeln:
Eine Node/Edge hat eine spezielle Länge
"------------" ist genauso lang wie eine andere Node/Edge

Zwei oder mehr Nodes/Edges müssen zusammen eine spezielle Länge haben
"------------------------" müssen so lange wie eine andere Node/Edge sein

Im Endeffekt wäre es besser, wenn man in der Liste später einfach eher geometrisch sieht, wie das Pflasterelement aussieht.
Vielleicht kann man hierbei auch abspeichern, wie man es zuletzt verändert hat. also keine audit-log, aber das dann einfach das Bild davon aktualisiert wird

In dieser Datei werden die "Regeln" jeder Familie gespeichert und es wird festgelegt, mit welchen Werten (Seitenlängen, Winkelgrößen) die Fünfecke erstellt werden.

Die jeweiligen Namen für die Winkel und Seiten geht mit dem Uhrzeigersinn

Man braucht drei aufeinander folgende Seitenlängen und alle Winkel um ein Fünfeck eindeutig zu bestimmen
*/

export default [
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'nobody',
        found: 2024,
        id: 0,
        init(length) {/*ONLY FOR FIRST APPEARANCE ON CANVAS*/
            this.angles = [105, 90, 145, 90, 110];
            this.strokes = [length, length, length, this.strokes[3], this.strokes[4]];
        },
        primitiveUnit: {count: 1, segmentToConnect: 0, offsets: []},
        strokes: [undefined, undefined, undefined, undefined, undefined],
        validateRules(angles, strokes) {
            let bool = true;
            for (let i = 0; i < angles.length; i++) {
                bool &= angles[i] > 50;
            }
            return equals(strokes[0], strokes[1]) && bool;
        },
    },
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'equalSides',
        equalSides: [0, 1, 2, 3, 4], //Wenn die Seiten n bis (n+1) immer gleich lang sein sollen, muss hier drin alles von n bis < (n+1) aufgelistet sein
        found: 2024,
        id: 0,
        init(length) {
            this.angles = [108, 108, 108, 108, 108];
            this.strokes = [length, length, length, this.strokes[3], this.strokes[4]];
        },
        primitiveUnit: {count: 1, segmentToConnect: 0, offsets: []},
        strokes: [undefined, undefined, undefined, undefined, undefined],
        validateRules(angles, strokes) {
            let bool = true;
            return equals(strokes[0], strokes[1]) && bool;
        },
    },
    //---------------------------------------------------------------
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'Reinhardt',
        found: 1918,
        id: 1,
        init(length) {
            this.angles = [120, 60, 120, 120, 120];
            /*all 3 variable*/
            this.strokes = [length, length * 2, length * 2, this.strokes[3], this.strokes[4]]
        },
        /*Farben in der Reihenfolge in der sie auf dem Canvas erscheinen*/
        primitiveUnit: {count: 1, segmentToConnect: 1, colors: ['yellow', 'lightblue']}, //purple auch gut
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined],
        validateRules(angles, strokes) {
            let sum1 = angles[0] + angles[1];
            let sum2 = angles[2] + angles[3] + angles[4];
            console.log(sum1, sum2);
            return equals(sum1, 180) &&
                equals(sum2, 360)
        },
    },
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'Reinhardt',
        found: 1918,
        id: 2,
        init(length) {
            this.angles = [120, 110, 90, 70, 150];
            this.strokes = [length * .8, length * .7, length, this.strokes[3], length]; //time for some change
            //this.strokes = [this.strokes[0], this.strokes[1], length, length*1., length];
        },
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined],
        primitiveUnit: {count: 4, colors: ['green','yellow','green','yellow']},
        validateRules(angles, strokes) {
            return equals(strokes[2], strokes[4]) &&

                equals(angles[1] + angles[3], 180)
        },
    },
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'Reinhardt',
        found: 1918,
        id: 3,
        init(length) {
            this.angles = [120, 100, 120, 120, 80];
            const c = length * 0.3;
            const e = length * 0.7;
            this.strokes = [this.strokes[0], this.strokes[1], c, c + e, e]
        },
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined], /*d=c+e, a=b (c+e) is min req.*/
        primitiveUnit: {count: 3, colors:['yellow','blue','orange']},
        validateRules(angles, strokes) {
            return equals(strokes[0], strokes[1]) &&
                equals(strokes[3], strokes[2] + strokes[4]) &&

                equals(angles[0], 120) &&
                equals(angles[2], 120) &&
                equals(angles[3], 120)
        },
    },
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'Reinhardt',
        found: 1918,
        id: 4,
        init(length) {
            this.angles = [150, 90, 130, 90, 80];
            this.strokes = [this.strokes[0], length * 0.59, length * 0.59, length, length]
        },
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined],
        primitiveUnit: {count: 4, colors:['yellow', 'grey', 'yellow', 'grey']},
        validateRules(angles, strokes) {
            return equals(strokes[1], strokes[2]) &&
                equals(strokes[3], strokes[4]) &&

                equals(angles[1], 90) &&
                equals(angles[3], 90)
        },
    },
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'Reinhardt',
        found: 1918,
        id: 5,
        init(length) {
            this.angles = [60, 120, 120, 120, 120];
            //this.strokes = [length, length, length * .2, this.strokes[3], this.strokes[4]]
            this.strokes = [length * 2, length * 2, this.strokes[3], length, length]
        },
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined],
        primitiveUnit: {count: 6, colors:['yellow','blue','orange']},
        validateRules(angles, strokes) {
            return equals(strokes[0], strokes[1]) &&
                equals(strokes[3], strokes[4]) &&

                equals(angles[0], 60) &&
                equals(angles[3], 120)
        },
    },
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'Kershner',
        found: 1968,
        id: 6,
        init(length) {
            this.angles = [135, 45, 135, 135, 90];
            this.strokes = [length, this.strokes[1], this.strokes[2], length, length]
        },
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined],
        primitiveUnit: {count: 4, colors:['yellow', 'yellow', 'blue', 'blue']},
        validateRules(angles, strokes) {
            return equals(strokes[0], strokes[3]) &&
                equals(strokes[3], strokes[4]) &&
                equals(strokes[1], strokes[2]) &&

                equals(angles[1] + angles[3], 180) &&
                equals(angles[1] * 2, angles[4])
        },
    },
    {
        //angles: [70, 125, 125, 110, 110],
        angles: [undefined, undefined, undefined, undefined, undefined],
        //angles: [60, 180, 120, 120, 90], //Hier soll B=D und E=C gelten
        explorer: 'Kershner',
        found: 1968,
        id: 7,
        init(length) {
            this.angles = [65, 140, 135, 90, 110];
            this.strokes = [this.strokes[0], length, length, length, length]
        },
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined],
        primitiveUnit: {count: 8, colors:['yellow', 'blue', 'lightblue', 'green','green','lightblue', 'blue', 'yellow']},
        validateRules(angles, strokes) {
            return equals(strokes[1], strokes[2]) &&
                equals(strokes[2], strokes[3]) &&
                equals(strokes[3], strokes[4]) &&

                equals(angles[1] + angles[4] * 2, 360) &&
                equals(angles[2] * 2 + angles[3], 360)
        },
    },
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'Kershner',
        found: 1968,
        id: 8,
        init(length) {
            this.angles = [85, 110, 125, 80, 140];
            this.strokes = [this.strokes[0], length, length, length, length]
        },
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined],
        primitiveUnit: {count: 8, colors:['yellow', 'yellow', 'lightblue', 'lightblue', 'green', 'green', 'blue','blue']},
        validateRules(angles, strokes) {
            return equals(strokes[1], strokes[2]) &&
                equals(strokes[2], strokes[3]) &&
                equals(strokes[3], strokes[4]) &&

                equals(angles[1] * 2 + angles[2], 360) &&
                equals(angles[4] * 2 + angles[3], 360)
        },
    },
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'Rice',
        found: [1975, 1976, 1977],
        id: 9,
        init(length) {
            this.angles = [100, 70, 160, 60, 150];
            this.strokes = [this.strokes[0], length, length, length, length]
        },
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined],
        primitiveUnit: {count: 8, colors:[]},
        validateRules(angles, strokes) {
            return equals(strokes[1], strokes[2]) &&
                equals(strokes[2], strokes[3]) &&
                equals(strokes[3], strokes[4]) &&

                equals(angles[0] * 2 + angles[2], 360) &&
                equals(angles[4] * 2 + angles[3], 360)
        },
    },
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'James',
        found: 1975,
        id: 10,
        init(length) {
            this.angles = [90, 100, 130, 140, 80];
            this.strokes = [length, length, length * .25, this.strokes[3], length * .75]
        },
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined], /*c+e=b=a c,e min. req*/
        primitiveUnit: {count: 6, colors:['red']}, //TODO: SPIEGELUNG
        validateRules(angles, strokes) {
            return equals(strokes[0], strokes[1]) &&
                equals(strokes[1], strokes[2] + strokes[4]) &&

                equals(angles[0], 90) &&
                equals(angles[1] + angles[4], 180) &&
                equals(angles[1] + angles[2] * 2, 360)
        },
    },
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'Rice',
        found: [1975, 1976, 1977],
        id: 11,
        init(length) {
            this.angles = [90, 145, 70, 125, 110];
            let a = length * .4;
            let c = length * .5;
            this.strokes = [a, this.strokes[1], c, 2 * a + c, 2 * a + c]
        },
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined], /*2a + c = d = e, a+c is min req*/
        primitiveUnit: {count: 8},
        validateRules(angles, strokes) {
            return equals(strokes[0] * 2 + strokes[2], strokes[3]) &&
                equals(strokes[3], strokes[4]) &&

                equals(angles[0], 90) &&
                equals(angles[2] + angles[4], 180) &&
                equals(angles[1] * 2 + angles[2], 360)
        },
    },
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'Rice',
        found: [1975, 1976, 1977],
        id: 12,
        init(length) {
            this.angles = [90, 150, 60, 120, 120];
            let c = length * .4;
            let e = length * .6;
            let d = c + e;
            this.strokes = [d / 2, this.strokes[1], c, d, e]
        },
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined], /*2a = d = c + e, c+e is min req*/
        primitiveUnit: {count: 8},
        validateRules(angles, strokes) {
            return equals(strokes[0] * 2, strokes[3]) &&
                equals(strokes[3], strokes[2] + strokes[4]) &&

                equals(angles[0], 90) &&
                equals(angles[2] + angles[4], 180) &&
                equals(angles[1] * 2 + angles[2], 360)
        },
    },
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'Rice',
        found: [1975, 1976, 1977],
        id: 13,
        init(length) {
            this.angles = [120, 90, 120, 120, 90];
            let l = length;
            this.strokes = [l/2, this.strokes[1], this.strokes[2], l, l/2]
        },
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined],
        primitiveUnit: {count: 8},
        validateRules(angles, strokes) {
            return equals(strokes[3], strokes[0] * 2) &&
                equals(strokes[3], strokes[4] * 2) &&

                equals(angles[1], 90) &&
                equals(angles[4], 90) &&
                equals(angles[0] * 2 + angles[3], 360)
        },
    },
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'Stein',
        found: 1985,
        id: 14,
        init(length) {
            this.angles = [90, 145.34, 69.32, 124.66, 110.68];
            this.strokes = [length,/*length*/ this.strokes[1], length, length*2, length*2]
            //this.strokes[1] *= Math.sqrt((11*Math.sqrt(57)-25)/8)
        },
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined],
        primitiveUnit: {count: 6},
        validateRules(angles, strokes) {
            return equals(strokes[0] * 2, strokes[2] * 2) &&
                equals(strokes[2] * 2, strokes[3]) &&
                equals(strokes[3], strokes[4]) &&

                equals(angles[0], 90) &&
                equals(angles[1], 145.34) &&
                equals(angles[2], 69.32) &&
                equals(angles[3], 124.66) &&
                equals(angles[4], 110.68)
        },
    },
    {
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'Mann-McLoud-Von Derau',
        found: 2015,
        id: 15,
        init(length) {
            this.angles = [150, 60, 135, 105, 90];
            this.strokes = [length, 2 * length, length, this.strokes[3]/*length*/, length]
            //this.strokes[3] *= (Math.sqrt(2))/(Math.sqrt(3)-1)
        },
        /*[A, B, C, D, E]*/
        strokes: [undefined, undefined, undefined, undefined, undefined],
        primitiveUnit: {count: 12},
        validateRules(angles, strokes) {
            return equals(strokes[0], strokes[2]) &&
                equals(strokes[2], strokes[4]) &&
                equals(strokes[1], strokes[0] * 2) &&

                equals(angles[0], 150) &&
                equals(angles[1], 60) &&
                equals(angles[2], 135) &&
                equals(angles[3], 105) &&
                equals(angles[4], 90)
        },
    }]

function equals(num1, num2, prec = 1) {
    return Math.abs(num1 - num2) < prec;
}

function undef() {
    return [undefined, undefined, undefined, undefined, undefined];
}
