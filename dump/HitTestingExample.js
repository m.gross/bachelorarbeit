var values = {
    paths: 1,
    points: 5,
    radius: 150
};

var hitOptions = {
    segments: true,
    stroke: true,
    fill: false, //DAMIT MAN PATH OBJEKTE NICHT BEWEGEN KANN, kann ich aber auf true setzen, dann wird halt der canvas "verschoben"
    tolerance: 5
};

createPaths();

function createPaths() {
    for (var i = 0; i < values.paths; i++) {
        var path = createPolygon();
        var lightness = (Math.random() - 0.5) * 0.4 + 0.4;
        var hue = Math.random() * 360;
        path.fillColor = {hue: hue, saturation: 1, lightness: lightness};
        path.strokeColor = 'black';
    };
}

function createPolygon() {
    var path = new Path();
    path.closed = true;
    for (var i = 0; i < points; i++) {
        var delta = new Point({
            length: (1 + Math.random()) * maxRadius * 0.5,
            angle: (360 / points) * i
        });
        path.add(view.center.add(delta));
    }
    return path;
}

var segment, path, pointBefore, pointAfter;
var movePath = false;

function onMouseDown(event) {
    console.log("down")
    console.log(event)

    segment = path = pointBefore = pointAfter = null;
    var hitResult = project.hitTest(event.point, hitOptions);
    if (!hitResult)
        return;

    if (hitResult) {
        path = hitResult.item;
        console.log(hitResult.type)
        if (hitResult.type == 'segment') {
            segment = hitResult.segment;
            console.log(segment)
        } else if (hitResult.type == 'stroke') {
            var pointOnStroke = hitResult.point;
            console.log("pointOnstroke" + pointOnStroke)

            //find segments before and after this stroke
            for (var i = 0; i < values.points; i++) {
                pointBefore = path.segments[i % values.points].point
                pointAfter = path.segments[(i + 1) % values.points].point

                var xxx = ((pointAfter.y - pointBefore.y) * (pointOnStroke.x - pointBefore.x))
                var yyy = ((pointAfter.x - pointBefore.x) * (pointOnStroke.y - pointBefore.y))

                if (Math.abs(xxx - yyy) <= 0.0001) {
                    console.log("Point " + pointBefore + " is behind, Point " + pointAfter + " is in front");
                    break;
                }
            }
        }
    }

    //muss vllt noch herausfinden, was der Block hier macht
    //movePath = hitResult.type == 'fill';
    if (movePath)
        project.activeLayer.addChild(hitResult.item);
}

function onMouseMove(event) {
    console.log("move")
    console.log(event)

    project.activeLayer.selected = false;
    if (event.item)
        event.item.selected = true;
}

//mouse drag ist also, wenn man auch einen knopf gedrückt hat
function onMouseDrag(event) {
    console.log("drag")
    console.log(event)

    var move = event.delta
    if (segment) {
        segment.point += move;
    } else if (pointBefore && pointAfter) {
        //stroke herausfinden
        //letzten und nächsten Punkt in die Richtung der Maus bewegen

        //Das muss noch besser gemacht werden, am besten so:
        //pointBefore.add(event.delta);
        //pointAfter.add(event.delta);

        pointBefore.x = pointBefore.x + event.delta.x;
        pointAfter.x = pointAfter.x + event.delta.x;

        pointBefore.y = pointBefore.y + event.delta.y;
        pointAfter.y = pointAfter.y + event.delta.y;
    }
}
