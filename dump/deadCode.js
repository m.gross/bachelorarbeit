const FamilyTileSchema = object({
      angles: array(number(), [
            every(angle => angle <= 180, "Not convex")
          ]
      ),
      strokes: array(number([])),
    },
    [
      forward(
          custom(
              (input) => true,//currentFamily.value.validateRules(input.angles, input.strokes) || true,
              'angles and strokes-validation'
          ), ['angles', 'strokes'],
      ),
    ],
);


//Das Array muss existieren
      if(currentFamily.value.equalSides) {
        for (let list of currentFamily.value.equalSides) {
          //TODO: Hier versuchen die beiden Dreiecktricks in Familie 12 anzuwenden
        }
        //Das aktuelle getroffene Segment muss darin enthalten sein
        if(currentFamily.value.equalSides.length === points.value) {
          equalSides(currentFamily.value.equalSides, move); //skaliert die jetzige Familie, falls halt keine anderen Operationen möglich sind
        }
      }


/**
 *
 * @param segments
 * @param move
 */
function equalSides(segments, move) {
  let cenPoint = calcCenter(pathHit);
  let expand = Math.abs(move.getDirectedAngle(segmentHit.point.subtract(cenPoint))) >= 90;
  let refVec = new Point(100, 0);

  for (let i of segments) { //Für die Indize, bei denen das gleich bleiben soll
    let point = pathHit.segments[i].point;
    let innerVector = point.subtract(cenPoint)
    let dir = refVec.getDirectedAngle(innerVector)

    pathHit.segments[i].point = point.add(new Point({length: move.length, angle: dir}).multiply(expand ? -1 : 1));
  }
}
