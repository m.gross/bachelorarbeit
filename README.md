# Bachelorarbeit Manuel Groß

Dieses Projekt beinhaltet meine Bachelorarbeit bestehend aus dem Programmcode für den praktischen Teil ([HITMan](HITMan)) 
und der tatsächlich geschriebenen [Arbeit](bachelorarbeit_manuel_groß.pdf).

Die Website besteht aus einem [Nuxt](https://nuxt.com/) Projekt mit [vue](https://vuejs.org/)-Komponenten, die ihre "Backend" Logik in Javascript umsetzen.
Für das Umsetzen des Canvas wird das JS-Framework [Paper.js](http://paperjs.org/) und zu Teilen [valibot](https://valibot.dev/) genutzt 

## Website - HITMan

In der README von [HITMan](HITMan) befindet sich eine Anleitung zur Inbetriebnahme. 

Aber hier ganz kurz: Generell kann man einen Packagemanager seiner Wahl benutzen.

Respository clonen: `git clone https://gitlab.ub.uni-bielefeld.de/m.gross/bachelorarbeit` 

In den Ordner wechseln: `cd bachelorarbeit/HITMan` (HITMan-Ordner: **WICHTIG**)

Alle nötigen Pakete installieren, mit npm z.B.: `npm install` 

HITMan bauen: `npm run build`

In dem erstellten `dist`-Ordner ist die website fertig, um gehostet zu werden.

Andernfalls kann mit `npm run dev` die Seite auch nur ausprobiert werden.

### Benutzung

Oben ist ein Dropdown-Menü, in dem alle 15 Familien aufgelistet sind, in der Reihenfolge, in der sie innerhalb der Liste aufgezählt sind. Der Name besteht aus dem/der Entdecker:in und dem Index innerhalb der Liste. Zusätzlich existiert ein Button zum Zurücksetzen (Reset), der alle Fünfecke im jetztigen Zustand neu auf dem Canvas anordnet und zentriert. Ein weiterer Knopf erlaubt das Verschieben von einzelnen Fünfeckinstanzen. Zu jeder Familie gehören Eingabefelder, über die bestimmte Parameter gesetzt werden können. 

Nachdem man eine Familie ausgewählt hat, kann man mit dem Mauszeiger über den Canvas fahren, um ein Fünfeck auszuwählen und es zu bewegen.


## Bachelorarbeit

Die pdf-Datei die hier im Repository zu finden ist entspricht der abgegeben Version an das Prüfungsamt, jedoch ohne meine Matrikelnummer. Persönliche Informationen sind hier also herausgenommen worden.

