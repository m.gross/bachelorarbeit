function computeValidPolygon(C, aC, c) {
  var B = { x: C.x + c,
            y: C.y };
  var D = { x: C.x + c * Math.cos(aC),
            y: C.y + c * Math.sin(aC) };
  var E = { x: D.x + c * Math.cos(Math.PI-aC),
            y: D.y + c * Math.sin(Math.PI-aC) };

  var dC = { x: 0, y: 0 };
  var dB = { x: 0, y: 0 };
  var dD = { x: 0, y: 0 };
  var dE = { x: 0, y: 0 };

  // starting value
  var aB = Math.PI/2;

  // target value
  var V3 = { x: 0, y: 0 };

  // tolerance (MSE)
  var tol = 0.01;

  do {
    // compute dependent values
    var A = { x: B.x + c * Math.cos(Math.PI-aB),
              y: B.y + c * Math.sin(Math.PI-aB) };
    var dA = { x:  c * Math.sin(Math.PI-aB),
               y: -c * Math.cos(Math.PI-aB) };

    var U = { x: Math.cos(Math.PI-aB/2-aC),
              y: Math.sin(Math.PI-aB/2-aC) };
    var dU = { x:  Math.sin(Math.PI-aB/2-aC)/2,
               y: -Math.cos(Math.PI-aB/2-aC)/2 };

    // (E-A) * U
    var V1 = (E.x-A.x) * U.x + (E.y-A.y) * U.y;
    // d(E-A) * U + (E-A) * dU
    var dV1 = (dE.x-dA.x) *  U.x + (dE.y-dA.y) *  U.y
            + ( E.x- A.x) * dU.x + ( E.y- A.y) * dU.y;

    // ((E-A) * U) * U
    var V2 = { x: V1 * U.x,
               y: V1 * U.y };
    var dV2 = { x: dV1 * U.x + V1 * dU.x,
                y: dV1 * U.y + V1 * dU.y };

    // projected point
    var V3 = { x: E.x - V2.x,
               y: E.y - V2.y };
    var dV3 = { x: dE.x - dV2.x,
                y: dE.y - dV2.y };

    // V4 > 0 => A is left of line
    var V4 = (V3.x-E.x) * (A.y-E.y) - (V3.y-E.y) * (A.x-E.x);
    var dV4 = (dV3.x-dE.x) * (A.y-E.y) + (V3.x-E.x) * (dA.y-dE.y)
            - (dV3.y-dE.y) * (A.x-E.x) - (V3.y-E.y) * (dA.x-dE.x);


    // Newton iteration
    aB -= V4 / dV4;
  } while(V4**2 > tol);

  console.log("A = x: " + A.x + " y: " + A.y);
  console.log("B = x: " + B.x + " y: " + B.y);
  console.log("C = x: " + C.x + " y: " + C.y);
  console.log("D = x: " + D.x + " y: " + D.y);
  console.log("E = x: " + E.x + " y: " + E.y);
  console.log("A' = x: " + V3.x + " y: " + V3.y);
  console.log("U: " + U.x + ", " + U.y);
  console.log("V1: " + V1);
  console.log("V2: " + V2.x + ", " + V2.y);

  return { points: [A, B, C, D, E], rule: "none" };
}

export { computeValidPolygon };
