import {Path} from "paper";

export class TilingPath extends Path {
    constructor(pathData) {
        super(pathData);
        this.pUnit = pathData.pUnit;
    }

    addPrimUnit(...tPath) {
        this.pUnit.push(...tPath);
    }

}