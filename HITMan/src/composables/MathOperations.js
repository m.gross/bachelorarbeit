//Hier sortiere ich sorgfältig alle Methoden rein, die nur Mathezeugs machen und nicht direkt bewegungslogik auf dem canvas ausführen

export const mod = (num, mod) => {
    //bei jeder anderen guten programmiersprache könnte man einfach %mod machen, aber hier nicht, danke JS
    num %= mod;
    num += num < 0 ? mod : 0;
    return num;
}

export const degreeToRadian = (degree) => {
    return (degree * Math.PI) / 180;
}

export const radianToDegree = (radian) => {
    return (radian * 180) / Math.PI;
}

/**
 *
 * @param A first sidelength
 * @param B second sidelength
 * @param gamma Angle in Degree
 * @returns {number} length of side opposite of gamma
 */
export const lawOfCosine = (A, B, gamma) => {
    return Math.sqrt(Math.pow(A, 2) + Math.pow(B, 2) - 2 * A * B * Math.cos(degreeToRadian(gamma)));
}