import families from './allFamilies.js'

export const useAllFamilies = () => {
    return families
};

export function getFamilyByExplorer(name) {
    return families.filter(x => x.explorer === name)[0]
}

export function getFamilyById(id) {
    return families.filter(x => x.id === id)[0]
}

export function getFamilyByCombinedName(name) {
    return families.filter(x => name === x.explorer + x.id)[0]
}


