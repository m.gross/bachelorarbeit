class Type7 {
  constructor(C, aC, c) {
    this.c = c;

    this.aC = aC;
    this.aD = 2*Math.pi-2*this.aC;

    this.C = C;
    this.B = { x: this.C.x + this.c,
               y: this.C.y };
    this.D = { x: this.C.x + this.c * Math.cos(this.aC),
               y: this.C.y + this.c * Math.sin(this.aC) };
    this.E = { x: this.D.x + this.c * Math.cos(Math.PI-this.aC),
               y: this.D.y + this.c * Math.sin(Math.PI-this.aC) };

    this.dC = { x: 0, y: 0 };
    this.dB = { x: 0, y: 0 };
    this.dD = { x: 0, y: 0 };
    this.dE = { x: 0, y: 0 };

    // initial value
    this.aB = Math.PI/2;
    this.update();
  }

  // updates the polygon after aB is changed
  update() {
    this.A = { x: this.B.x + this.c * Math.cos(Math.PI-this.aB),
               y: this.B.y + this.c * Math.sin(Math.PI-this.aB) };
    this.dA = { x:  this.c * Math.sin(Math.PI-this.aB),
                y: -this.c * Math.cos(Math.PI-this.aB) };

    // target direction vector from E to A
    this.U = { x: Math.cos(Math.PI-this.aB/2-this.aC),
               y: Math.sin(Math.PI-this.aB/2-this.aC) };
    this.dU = { x:  Math.sin(Math.PI-this.aB/2-this.aC)/2,
                y: -Math.cos(Math.PI-this.aB/2-this.aC)/2 };

    // (E-A) * U
    var V1 = (this.E.x-this.A.x) * this.U.x + (this.E.y-this.A.y) * this.U.y;
    // d(E-A) * U + (E-A) * dU
    var dV1 = (this.dE.x-this.dA.x) * this.U.x  + (this.dE.y-this.dA.y) * this.U.y
            + (this.E.x -this.A.x)  * this.dU.x + (this.E.y -this.A.y)  * this.dU.y;

    // ((E-A) * U) * U
    var V2 = { x: V1 * this.U.x,
               y: V1 * this.U.y };
    var dV2 = { x: dV1 * this.U.x + V1 * this.dU.x,
                y: dV1 * this.U.y + V1 * this.dU.y };

    // projected point
    this.AP = { x: this.E.x - V2.x,
                y: this.E.y - V2.y };
    this.dAP = { x: this.dE.x - dV2.x,
                 y: this.dE.y - dV2.y };

    // V4 > 0 => A is left of line
    this.V4 = (this.AP.x-this.E.x) * (this.A.y-this.E.y) - (this.AP.y-this.E.y) * (this.A.x-this.E.x);
    this.dV4 = (this.dAP.x-this.dE.x) * (this.A.y-this.E.y) + (this.AP.x-this.E.x) * (this.dA.y-this.dE.y)
             - (this.dAP.y-this.dE.y) * (this.A.x-this.E.x) - (this.AP.y-this.E.y) * (this.dA.x-this.dE.x);
  }

  newtonIteration() {
    this.aB -= this.V4 / this.dV4;
    this.update();
  }

  solve() {
    console.log(this.aB);
    while(this.V4**2 > 0.00001) {
      this.newtonIteration();
      console.log(this.aB);
    }
  }

  change(A) {
    // computing dB from A
    var a = this.c;
    var b2 = (A.x-this.C.x)**2 + (A.y-this.C.y)**2;
    var c = this.c;
    var aB = Math.acos((a**2+c**2-b2) / (2*a*c));

    this.aB = aB;
    this.update();
  }

  computeAngles() {
    var ac2 = (this.A.x-this.C.x)**2 + (this.A.y-this.C.y)**2;
    var bd2 = (this.B.x-this.D.x)**2 + (this.B.y-this.D.y)**2;
    var ce2 = (this.C.x-this.E.x)**2 + (this.C.y-this.E.y)**2;
    var da2 = (this.D.x-this.A.x)**2 + (this.D.y-this.A.y)**2;
    var eb2 = (this.E.x-this.B.x)**2 + (this.E.y-this.B.y)**2;

    var ae = Math.sqrt((this.A.x-this.E.x)**2 + (this.A.y-this.E.y)**2);
    var c = this.c

    var aA = Math.acos((ae**2+c**2-eb2) / (2*ae*c));
    var aB = Math.acos((2*c**2-ac2) / (2*c**2));
    var aC = Math.acos((2*c**2-bd2) / (2*c**2));
    var aD = Math.acos((2*c**2-ce2) / (2*c**2));
    var aE = Math.acos((ae**2+c**2-da2) / (2*ae*c));

    return { aA: aA/Math.PI*180,
             aB: aB/Math.PI*180,
             aC: aC/Math.PI*180,
             aD: aD/Math.PI*180,
             aE: aE/Math.PI*180 };
  }
}

export { Type7 };
