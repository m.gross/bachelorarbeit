import { Type7 } from "./type7.js"

var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");

var C = { x: 100, y: 100 };
var aC = 150/180*Math.PI;
var c = 100;
var polygon = new Type7(C, aC, c);
polygon.solve();
var angles = polygon.computeAngles();
console.log(angles.aA, angles.aB, angles.aC, angles.aD, angles.aE);

var points = [{x:95,y:40,fixed:false}]
var polygons = [];

var dragging = false;
var draggingType7 = false;
var draggedPoint = null;

function drawPoint(point) {
  ctx.beginPath();
  ctx.arc(point.x, point.y, 5, 0, 2 * Math.PI);
  ctx.stroke();
  if(point.fixed) {
    ctx.fillStyle = "grey";
    ctx.fill();
  }
}

function drawLine(point1, point2) {
  ctx.moveTo(point1.x, point1.y);
  ctx.lineTo(point2.x, point2.y);
  ctx.stroke();
}

function drawPolygon(polygon) {
  var numPoints = polygon.points.length;
  polygon.points.forEach(drawPoint);
  for(var i = 1; i < numPoints; i++) {
    drawLine(polygon.points[i-1], polygon.points[i]);
  }
  drawLine(polygon.points[numPoints-1], polygon.points[0]);
}

function drawType7(polygon) {
  drawPoint(polygon.A);
  drawPoint(polygon.B);
  drawPoint(polygon.C);
  drawPoint(polygon.D);
  drawPoint(polygon.E);
  drawPoint(polygon.AP);

  drawLine(polygon.A, polygon.B);
  drawLine(polygon.B, polygon.C);
  drawLine(polygon.C, polygon.D);
  drawLine(polygon.D, polygon.E);
}

function drawPoints() {
  points.forEach(drawPoint);
}

function drawPolygons() {
  polygons.forEach(drawPolygon);
}

function updateCanvas() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  //drawPoints();
  drawPolygons();
  drawType7(polygon);
}

// project x on line r + lambda * u
function projection(r, u, x) {
  var lambda = ((x.x-r.x)*u.x + (x.y-r.y)*u.x) / (u.x*u.x + u.y*u.y);
  return { x: r.x+lambda*u.x, y: r.y+lambda*u.y };
}

function updatePolygon(polygon) {
  if(polygon.rule === "parallelogram") {
    polygon.points[2].x = polygon.points[1].x - polygon.points[0].x + polygon.points[3].x;
    polygon.points[2].y = polygon.points[1].y - polygon.points[0].y + polygon.points[3].y;
  } else if(polygon.rule === "reinhardt1") {
    var u = { x: polygon.points[4].x-polygon.points[1].x, y: polygon.points[4].y-polygon.points[1].y };
    var p = projection(polygon.points[3], u, polygon.points[2]);
    polygon.points[2].x = p.x;
    polygon.points[2].y = p.y;
  }
}

function handleMouseUp(e) {
  dragging = false;
  draggingType7 = false;
  draggedPoint = null;
}

function handleMouseDown(e) {
  var rect = canvas.getBoundingClientRect();
  var mouseX = parseInt(e.clientX-rect.left);
  var mouseY = parseInt(e.clientY-rect.top);

  points.forEach(point => {
    var distance = (point.x - mouseX) ** 2 + (point.y - mouseY) ** 2;
    if(distance <= 25 && !point.fixed) {
      dragging = true;
      draggedPoint = point;
      return;
    }
  });
  polygons.forEach(polygon => {
    polygon.points.forEach(point => {
      var distance = (point.x - mouseX) ** 2 + (point.y - mouseY) ** 2;
      if(distance <= 25 && !point.fixed) {
        dragging = true;
        draggedPoint = point;
        return;
      }
    });
  });
  var point = polygon.A;
  var distance = (point.x - mouseX) ** 2 + (point.y - mouseY) ** 2;
  if(distance <= 25) {
    dragging = true;
    draggingType7 = true;
    return;
  }
}

function handleMouseMove(e) {
  if(dragging) {
    var rect = canvas.getBoundingClientRect();
    var mouseX = parseInt(e.clientX-rect.left);
    var mouseY = parseInt(e.clientY-rect.top);

    if(draggingType7) {
      polygon.change({x: mouseX, y: mouseY});
    } else {
      draggedPoint.x = mouseX;
      draggedPoint.y = mouseY;
      polygons.forEach(updatePolygon);
    }
    updateCanvas();
  }
}

canvas.addEventListener("mouseup", handleMouseUp);
canvas.addEventListener("mousedown", handleMouseDown);
canvas.addEventListener("mousemove", handleMouseMove);

updateCanvas();
