/* Arten von Regeln:
Im Endeffekt wäre es besser, wenn man in der Liste später einfach eher geometrisch sieht, wie das Pflasterelement aussieht.
Vielleicht kann man hierbei auch abspeichern, wie man es zuletzt verändert hat. also keine audit-log, aber das dann einfach das Bild davon aktualisiert wird

In dieser Datei werden die "Regeln" jeder Familie gespeichert und es wird festgelegt, mit welchen Werten (Seitenlängen, Winkelgrößen) die Fünfecke erstellt werden.

Die jeweiligen Namen für die Winkel und Seiten geht mit dem Uhrzeigersinn*/


//viele Familien haben unterschiedliche Tupel die sie eindeutig bestimmen, aber auch
const UNDEF = undefined;
//{label:"", value:0},
export default [
    {
        parameters: [{label: "Seitenlänge a:", value: 100}],/* {label: "Seitenlänge b:", value: 200}, {
            label: "Seitenlänge c:",
            value: 200
        }, {label: "Winkel A:", value: 120}, {label: "Winkel B", value: 60}, {
            label: "Winkel E:",
            value: 120
        }],*/
        info: "Mindestanforderungen: Drei Seiten hintereinander und drei Winkel, " +
            "von denen zwei aus (A, D, E) und einer aus (B, C) kommt ",
        angles: [undefined, undefined, 120, 120, undefined],
        explorer: 'Reinhardt',
        found: 1918,
        id: 1,
        init() {
            let A = 120; //this.parameters[3].value;
            let B = 60; ///this.parameters[4].value;
            let E = 120; //this.parameters[5].value;
            this.angles = [A, B, 180 - B, 360 - A - E, E];
            /*all 3 variable*/
            let a = this.parameters[0].value;
            this.strokes = [a, 2 * a, 2 * a, UNDEF, UNDEF]
        },
        /*Farben in der Reihenfolge in der sie auf dem Canvas erscheinen*/
        primitiveUnit: {
            clockwise: [true, true],
            colors: ['yellow', 'blue'], //purple auch gut
            connection: [
                [], //first object isn't being connected anywhere
                [1, 0], //segment previous path, segment next path//indices of the previous path to connect the new path to
            ],
            count: 2,
            rotation: [
                {s1s1: UNDEF, s1s2: UNDEF, s2s1: UNDEF, s2s2: UNDEF}, //first object can be rotated, if desired
                {s1s1: 1, s1s2: 2, s2s1: 4, s2s2: 0},
                /*rotation der ersten kachel, falls das aber gewünscht ist, ist das hier eingefügt*/
                /*segmentindize mit deren Winkeln rotiert wird*/
            ],
            offset: [
                {childId1: 0, segment1: 2, childId2: 1, segment2: 3},
                {childId1: 0, segment1: 3, childId2: 1, segment2: 4},
                {childId1: 1, segment1: 2, childId2: 1, segment2: 4},

                {childId1: 1, segment1: 2, childId2: 0, segment2: 3},
                {childId1: 1, segment1: 3, childId2: 0, segment2: 4},
                {childId1: 1, segment1: 4, childId2: 1, segment2: 2},
            ]
        },
        strokes: [],
    },
    {
        parameters: [{label: "Seitenlänge c und d:", value: 100}],
        angles: [120, 110, 90, 70, 150],
        //angles: [134.4524, 110, 89.999, 70, 135.5475],
        explorer: 'Reinhardt',
        found: 1918,
        id: 2,
        init() {
            let c = this.parameters[0].value;
            this.strokes = [c * .8, c * .7, c, UNDEF, c];
            this.angles = [120, 110, 90, 70, 150];
        },
        primitiveUnit: {
            clockwise: [true, false, true, false],
            colors: ['green', 'yellow', 'green', 'yellow'],
            connection: [
                /*erster index beschreibt das fünfeck, das ... wird, zweiter index beschreibt das fünfeck, das ... wird*/
                [2, 0],
                [1, 3],
                [2, 0],
                [1, 3],
            ],
            count: 4,
            //entweder die erstellung läuft "ringsherum" oder es wird in jede richtung eine feste anzahl an elementen erstellt
            offset: [ //in wie viele richtungen verschoben werden und wie der offset berechnet wird
                {childId1: 0, segment1: 0, childId2: 2, segment2: 4},
                {childId1: 2, segment1: 0, childId2: 0, segment2: 4},

                {childId1: 0, segment1: 1, childId2: 1, segment2: 3},
                {childId1: 1, segment1: 3, childId2: 0, segment2: 1},
            ],
            rotation: [
                /*stroke1 wird ... und stroke2 wird ...*/
                {s1s1: 2, s1s2: 4, s2s1: 2, s2s2: 3},
                {s1s1: 1, s1s2: 2, s2s1: 3, s2s2: 4},
                {s1s1: 0, s1s2: 1, s2s1: 2, s2s2: 3},
                {s1s1: 1, s1s2: 2, s2s1: 3, s2s2: 4},
            ],
        },
        strokes: [],
    },
    {
        parameters: [{label: "Seitenlänge c:", value: 70}, {label: "Seitenlänge e:", value: 30},],
        angles: [120, undefined, 120, 120, undefined],
        explorer: 'Reinhardt',
        found: 1918,
        id: 3,
        init() {
            const c = this.parameters[0].value;
            const e = this.parameters[1].value;
            this.strokes = [UNDEF, UNDEF, c, c + e, e]
            this.angles[0] = 120;
            this.angles[2] = 120;
            this.angles[3] = 120;
        },
        primitiveUnit: {
            clockwise: [true, true, true],
            colors: ['yellow', 'blue', 'orange'],
            connection: [
                [],
                [0, 0],
                [0, 0],
            ],
            count: 3,
            offset: [
                {childId1: 0, segment1: 2, childId2: 1, segment2: 2},
                {childId1: 1, segment1: 2, childId2: 0, segment2: 2},

                {childId1: 1, segment1: 2, childId2: 2, segment2: 2},
                {childId1: 2, segment1: 2, childId2: 1, segment2: 2},

                {childId1: 2, segment1: 2, childId2: 0, segment2: 2},
                {childId1: 0, segment1: 2, childId2: 2, segment2: 2},
            ],
            rotation: [
                {s1s1: UNDEF, s1s2: UNDEF, s2s1: UNDEF, s2s2: UNDEF},
                {s1s1: 0, s1s2: 4, s2s1: 0, s2s2: 1},
                {s1s1: 0, s1s2: 4, s2s1: 0, s2s2: 1},
            ],
        }, /*d=c+e, a=b (c+e) is min req.*/
        strokes: [],
    },
    {
        parameters: [{label: "Seitenlänge c und d:", value: 100}],
        angles: [150, 90, 130, 90, 80],
        //angles: [undefined, undefined, undefined, 90, undefined], //NEW
        explorer: 'Reinhardt',
        found: 1918,
        id: 4,
        init() {
            let c = this.parameters[0].value;
            this.strokes = [UNDEF, c * 0.59, c * 0.59, c, c]
            this.angles = [150, 90, 130, 90, 80];
        },
        primitiveUnit: {
            clockwise: [true, true, true, true],
            colors: ['yellow', 'grey', 'yellow', 'grey'],
            connection: [
                [],
                [3, 3],
                [2, 4],
                [3, 3],
            ],
            count: 4,
            offset: [
                {childId1: 1, segment1: 0, childId2: 3, segment2: 4},
                {childId1: 3, segment1: 4, childId2: 1, segment2: 0},

                {childId1: 2, segment1: 4, childId2: 0, segment2: 0},
                {childId1: 0, segment1: 0, childId2: 2, segment2: 4},

                {childId1: 1, segment1: 1, childId2: 3, segment2: 1},
                {childId1: 3, segment1: 1, childId2: 1, segment2: 1},

                {childId1: 2, segment1: 1, childId2: 0, segment2: 1},
                {childId1: 0, segment1: 1, childId2: 2, segment2: 1},
            ],
            rotation: [
                /*s1s1:,s1s2:,s2s1:,s2s2:*/
                {s1s1: UNDEF, s1s2: UNDEF, s2s1: UNDEF, s2s2: UNDEF},
                {s1s1: 3, s1s2: 4, s2s1: 2, s2s2: 3},
                {s1s1: 3, s1s2: 4, s2s1: 2, s2s2: 3},
                {s1s1: 3, s1s2: 4, s2s1: 2, s2s2: 3},
            ],
        },
        strokes: [],
    },
    {
        parameters: [{label: "Seitenlänge c und d:", value: 100}],
        angles: [60, 120, 120, 120, 120],
        //angles: [60, undefined, undefined, 120, undefined],//NEW
        explorer: 'Reinhardt',
        found: 1918,
        id: 5,
        init() {
            let c = this.parameters[0].value;
            //this.strokes = [length, length, length * .2, this.strokes[3], this.strokes[4]]
            this.strokes = [c * 2, c * 2, UNDEF, c, c]
            this.angles = [60, 120, 120, 120, 120];
        },
        primitiveUnit: {
            clockwise: [],
            //colors: ['yellow', 'blue', 'yellow', 'blue', 'yellow', 'blue'],
            colors: ['yellow', 'blue', 'orange', 'yellow', 'blue', 'orange'],
            connection: [
                [],
                [0, 0],
                [0, 0],
                [0, 0],
                [0, 0],
                [0, 0],
            ],
            count: 6,
            offset: [
                {childId1: 0, segment1: 3, childId2: 3, segment2: 4},
                {childId1: 3, segment1: 4, childId2: 0, segment2: 3},

                {childId1: 1, segment1: 3, childId2: 4, segment2: 4},
                {childId1: 4, segment1: 4, childId2: 1, segment2: 3},

                {childId1: 2, segment1: 3, childId2: 5, segment2: 4},
                {childId1: 5, segment1: 4, childId2: 2, segment2: 3},
            ],
            rotation: [
                {s1s1: UNDEF, s1s2: UNDEF, s2s1: UNDEF, s2s2: UNDEF},
                {s1s1: 0, s1s2: 1, s2s1: 0, s2s2: 4},
                {s1s1: 0, s1s2: 1, s2s1: 0, s2s2: 4},
                {s1s1: 0, s1s2: 1, s2s1: 0, s2s2: 4},
                {s1s1: 0, s1s2: 1, s2s1: 0, s2s2: 4},
                {s1s1: 0, s1s2: 1, s2s1: 0, s2s2: 4},
            ],
        },
        strokes: [],
    },

    {
        parameters: [{label: "Seitenlänge a, e und d:", value: 100}, {label: "Winkel B (min 30):", value: 40}],
        angles: [undefined, 40, undefined, 140, 80], //NEW
        explorer: 'Kershner',
        found: 1968,
        id: 6,
        init() {
            let length = this.parameters[0].value;
            this.angles[1] = this.parameters[1].value;
            this.angles[4] = 2 * this.parameters[1].value;
            this.angles[3] = 180 - this.parameters[1].value;
            this.strokes = [length, UNDEF, UNDEF, length, length]
        },
        primitiveUnit: {
            clockwise: [true, true, true, true],
            colors: ['yellow', 'yellow', 'blue', 'blue'],
            connection: [
                [],
                [1, 2],
                [4, 3],
                [1, 0],
            ],
            count: 4,
            offset: [
                {childId1: 0, segment1: 2, childId2: 3, segment2: 0},
                {childId1: 3, segment1: 0, childId2: 0, segment2: 2},

                {childId1: 1, segment1: 0, childId2: 0, segment2: 4},
                {childId1: 0, segment1: 4, childId2: 1, segment2: 0},

                {childId1: 0, segment1: 3, childId2: 2, segment2: 3},
                {childId1: 2, segment1: 3, childId2: 0, segment2: 3},
            ],
            rotation: [
                {s1s1: 0, s1s2: 1, s2s1: 1, s2s2: 2},
                {s1s1: 2, s1s2: 1, s2s1: 1, s2s2: 2},
                {s1s1: 4, s1s2: 0, s2s1: 3, s2s2: 2},
                {s1s1: 1, s1s2: 0, s2s1: 0, s2s2: 1},
            ],
        },
        strokes: [],
    },

    {
        parameters: [
            {label: "Seitenlänge b, c, d und e:", value: 100},
            {label: "Winkel A:", value: 70},
            {label: "Winkel C:", value: 135.5}
        ],
        info: "A, C und c wird gegeben",
        angles: [UNDEF, UNDEF, UNDEF, UNDEF, UNDEF],
        explorer: 'Kershner',
        found: 1968,
        id: 7,
        init() {
            let length = this.parameters[0].value;
            this.strokes = [UNDEF, length, length, length, length];

            this.angles[0] = 70;
            this.angles[2] = this.parameters[2].value;
            this.angles[3] = 360 - (2 * this.angles[2]);
        },

        strokes: [],
        primitiveUnit: {
            clockwise: [true, false, true, false, false, true, false, true],
            colors: ['yellow', 'blue', 'purple', 'green', 'green', 'purple', 'blue', 'yellow'],
            connection: [
                [],
                [4, 4],
                [3, 0],
                [0, 0],
                [3, 2],
                [0, 0],
                [0, 3],
                [4, 4],
            ],
            offset: [
                {childId1: 3, segment1: 0, childId2: 4, segment2: 1},
                {childId1: 4, segment1: 1, childId2: 3, segment2: 0},

                {childId1: 2, segment1: 3, childId2: 5, segment2: 2},
                {childId1: 5, segment1: 2, childId2: 2, segment2: 3},
            ],
            count: 8,
            rotation: [
                {s1s1: UNDEF, s1s2: UNDEF, s2s1: UNDEF, s2s2: UNDEF},
                {s1s1: 4, s1s2: 0, s2s1: 4, s2s2: 0},
                {s1s1: 3, s1s2: 4, s2s1: 0, s2s2: 1},
                {s1s1: 0, s1s2: 4, s2s1: 0, s2s2: 4},
                {s1s1: 3, s1s2: 2, s2s1: 2, s2s2: 3},
                {s1s1: 0, s1s2: 4, s2s1: 0, s2s2: 4},
                {s1s1: 3, s1s2: 4, s2s1: 0, s2s2: 1},
                {s1s1: 4, s1s2: 0, s2s1: 4, s2s2: 0},
            ],
        },
    },
    {
        parameters: [
            {label: "Seitenlänge b, c, d und e:", value: 100},
            {label: "Winkel A:", value: 98},
            {label: "Winkel C:", value: 110}
        ],
        info: "C und D werden gegeben",
        angles: [UNDEF, UNDEF, UNDEF, UNDEF, UNDEF],
        explorer: 'Kershner',
        found: 1968,
        id: 8,
        init() {
            let length = this.parameters[0].value;
            this.strokes = [UNDEF, length, length, length, length];
            this.angles[0] = this.parameters[1].value;
            this.angles[2] = this.parameters[2].value;
            this.angles[1] = 360 - this.angles[2];
            this.angles[1] /= 2;
        },
        strokes: [],
        primitiveUnit: {
            clockwise: [false, false, true, true, true, true, false, false],
            colors: ['blue', 'blue', 'yellow', 'yellow', 'lightblue', 'lightblue', 'green', 'green'],
            connection: [
                [],
                [3, 4],
                [4, 4],
                [2, 1],
                [0, 3],
                [3, 4],
                [0, 0],
                [2, 1],

            ],
            offset: [
                {childId1: 7, segment1: 2, childId2: 0, segment2: 1},
                {childId1: 0, segment1: 1, childId2: 7, segment2: 2},

                {childId1: 0, segment1: 4, childId2: 3, segment2: 4},
                {childId1: 3, segment1: 4, childId2: 0, segment2: 4},

                {childId1: 3, segment1: 0, childId2: 7, segment2: 3},
                {childId1: 7, segment1: 3, childId2: 3, segment2: 0},

                {childId1: 0, segment1: 0, childId2: 5, segment2: 2},
                {childId1: 5, segment1: 2, childId2: 0, segment2: 0},
            ],
            count: 8,
            rotation: [
                {s1s1: 4, s1s2: 0, s2s1: 0, s2s2: 4},
                {s1s1: 4, s1s2: 3, s2s1: 3, s2s2: 4},
                {s1s1: 4, s1s2: 0, s2s1: 4, s2s2: 0},
                {s1s1: 2, s1s2: 1, s2s1: 1, s2s2: 2},
                {s1s1: 0, s1s2: 1, s2s1: 3, s2s2: 2},
                {s1s1: 3, s1s2: 4, s2s1: 4, s2s2: 3},
                {s1s1: 0, s1s2: 4, s2s1: 0, s2s2: 4},
                {s1s1: 2, s1s2: 1, s2s1: 1, s2s2: 2},

            ],
        },
    },
    {
        parameters: [
            {label: "Seitenlänge b, c, d und e:", value: 100},
            {label: "Winkel B:", value: 142},
            {label: "Winkel C:", value: 65}
        ],
        info: "C und D werden gegeben",
        angles: [UNDEF, UNDEF, UNDEF, UNDEF, UNDEF],
        explorer: 'Rice',
        found: [1975, 1976, 1977],
        id: 9,
        init() {
            let length = this.parameters[0].value;
            this.strokes = [UNDEF, length, length, length, length]

            this.angles[2] = this.parameters[1].value;
            this.angles[3] = this.parameters[2].value;

            this.angles[0] = (360 - this.angles[2]) / 2;
            this.angles[4] = (360 - this.angles[3]) / 2;

            this.angles[1] = this.angles[0] + this.angles[2] + this.angles[3] + this.angles[4];
        },
        primitiveUnit: {
            clockwise: [true, false, false, true, false, true, true, false],
            colors: ['yellow', 'purple', 'purple', 'yellow', 'green', 'blue', 'blue', 'green'],
            connection: [
                [],
                [0, 0],
                [3, 4],
                [4, 4],
                [3, 1],
                [0, 0],
                [4, 3],
                [4, 4],
            ],
            offset: [
                {childId1: UNDEF, segment1: UNDEF, childId2: UNDEF, segment2: UNDEF},
                {childId1: UNDEF, segment1: UNDEF, childId2: UNDEF, segment2: UNDEF},

                {childId1: UNDEF, segment1: UNDEF, childId2: UNDEF, segment2: UNDEF},
                {childId1: UNDEF, segment1: UNDEF, childId2: UNDEF, segment2: UNDEF},

                {childId1: UNDEF, segment1: UNDEF, childId2: UNDEF, segment2: UNDEF},
                {childId1: UNDEF, segment1: UNDEF, childId2: UNDEF, segment2: UNDEF},
            ],
            count: 8,
            rotation: [
                {s1s1: 4, s1s2: 0, s2s1: 4, s2s2: 3},
                {s1s1: 0, s1s2: 4, s2s1: 0, s2s2: 4},
                {s1s1: 3, s1s2: 4, s2s1: 4, s2s2: 3},
                {s1s1: 4, s1s2: 0, s2s1: 4, s2s2: 0},
                {s1s1: 1, s1s2: 0, s2s1: 3, s2s2: 2},
                {s1s1: 0, s1s2: 4, s2s1: 0, s2s2: 4},
                {s1s1: 4, s1s2: 3, s2s1: 3, s2s2: 4},
                {s1s1: 4, s1s2: 0, s2s1: 4, s2s2: 0},
            ],
        },
        strokes: [],
    },

    {
        parameters: [
            {label: "Seite c:", value: 100, disabled:true},
            {label: "Seite e:", value: 100, disabled:true},
            {label: "Winkel E:", value: 90, disabled:true}
        ],
        angles: [90, 100, 130, 140, 80], //verworfen, weil geringe abweichungen
        explorer: 'James',
        found: 1975,
        id: 10,
        init() {
            let c = this.parameters[0].value;
            let e = this.parameters[1].value;
            this.strokes = [c + e, c + e, c, UNDEF, e];

            let E = this.parameters[2].value;
            let B = 180 - E;
            let C = 180 - (B / 2);

            this.angles = [90, B, C, UNDEF, E];
            this.angles[3] = 450 - B - C - E;
        },
        strokes: [], /*c+e=b=a c,e min. req*/
        primitiveUnit: {
            clockwise: [true, false, false, false, false, true],
            colors: ['red', 'blue', 'yellow', 'yellow', 'blue', 'red'],
            connection: [
                [],
                [2, 2],
                [0, 0],
                [0, 0],
                [0, 0],
                [2, 2],
            ], //TODO: SPIEGELUNG
            count: 6,
            offset: [
                {childId1: 0, segment1:1, childId2: 5, segment2:2},
                {childId1: 5, segment1:2, childId2: 0, segment2:1},
                {childId1: 0, segment1:3, childId2: 5, segment2:4},
                {childId1: 5, segment1:4, childId2: 0, segment2:3},

                {childId1: 2, segment1:2, childId2: 3, segment2:3},
                {childId1: 3, segment1:3, childId2: 2, segment2:2},
            ],
            rotation: [
                {s1s1: UNDEF, s1s2: UNDEF, s2s1: UNDEF, s2s2: UNDEF},
                {s1s1: 2, s1s2: 3, s2s1: 2, s2s2: 3},
                {s1s1: 0, s1s2: 1, s2s1: 0, s2s2: 4},
                {s1s1: 0, s1s2: 1, s2s1: 1, s2s2: 0},
                {s1s1: 0, s1s2: 4, s2s1: 0, s2s2: 1},
                {s1s1: 2, s1s2: 3, s2s1: 2, s2s2: 3},
            ],
        },
    },
    {
        parameters: [{label: "Seitenlänge a:", value: 25}, {label: "Seitenlänge c:", value: 50}, {
            label: "Winkel C:",
            value: 70
        }],
        angles: [90, 145, 70, 125, 110],
        explorer: 'Rice',
        found: [1975, 1976, 1977],
        id: 11,
        init() {
            let a = this.parameters[0].value;
            let c = this.parameters[1].value;
            this.strokes = [a, UNDEF, c, 2 * a + c, 2 * a + c];

            let C = this.parameters[2].value;
            this.angles = [90, (360 - C) / 2, C, UNDEF, 180 - C];
            this.angles[3] = 450 - (this.angles[1] + this.angles[2]+this.angles[4]) //TODO: Durch Berechnung ersetzen
        },
        strokes: [],
        primitiveUnit: {
            clockwise: [false, true, true, false, false, true, true, false],
            colors: ['purple', 'yellow', 'yellow', 'purple', 'green', 'blue', 'blue', 'green'],
            connection: [
                [],
                [0, 0],
                [3, 4],
                [0, 0],
                [3, 3],
                [1, 1],
                [1, 2],
                [0, 0],
            ],
            offset: [
                {childId1: UNDEF, segment1: UNDEF, childId2: UNDEF, segment2: UNDEF},
                {childId1: UNDEF, segment1: UNDEF, childId2: UNDEF, segment2: UNDEF},
            ],
            count: 8,
            rotation: [
                {s1s1: 2, s1s2: 1, s2s1: 4, s2s2: 0},
                {s1s1: 0, s1s2: 1, s2s1: 0, s2s2: 1},
                {s1s1: 3, s1s2: 4, s2s1: 4, s2s2: 3},
                {s1s1: 0, s1s2: 1, s2s1: 0, s2s2: 1},
                {s1s1: 3, s1s2: 2, s2s1: 3, s2s2: 4},
                {s1s1: 1, s1s2: 0, s2s1: 1, s2s2: 0},
                {s1s1: 1, s1s2: 2, s2s1: 2, s2s2: 1},
                {s1s1: 0, s1s2: 1, s2s1: 0, s2s2: 1},
            ],
        },
    },
    {
        parameters: [
            {label: "Seitenlänge c:", value: 40},
            {label: "Seitenlänge e:", value: 60},
            {label: "Winkel C:", value: 60},
        ],
        angles: [90, 150, 60, 120, 120],
        explorer: 'Rice',
        found: [1975, 1976, 1977],
        id: 12,
        init() {
            this.angles[0] = 90;
            this.angles[2] = this.parameters[2].value;
            this.angles[1] = (360 - this.angles[2]) / 2;
            this.angles[4] = (180 - this.angles[2]);
            this.angles[3] = 120;

            let c = this.parameters[0].value;
            let e = this.parameters[1].value;
            let d = c + e;
            this.strokes = [d / 2, UNDEF, c, d, e]
        },
        strokes: [], /*2a = d = c + e, c+e is min req*/
        primitiveUnit: {
            clockwise: [false, true, true, false, false, true, false, true],
            colors: ['limegreen', 'blue', 'blue', 'limegreen', 'purple', 'yellow', 'purple', 'yellow'],
            connection: [
                [],
                [0, 0],
                [2, 1],
                [1, 1],
                [4, 3],
                [0, 0],
                [2, 1],
                [1, 1],
            ],
            count: 8,
            offset: [
                {childId1: UNDEF, segment1: UNDEF, childId2: UNDEF, segment2: UNDEF},
                {childId1: UNDEF, segment1: UNDEF, childId2: UNDEF, segment2: UNDEF},
            ],
            rotation: [
                {s1s1: UNDEF, s1s2: UNDEF, s2s1: UNDEF, s2s2: UNDEF},
                {s1s1: 0, s1s2: 1, s2s1: 0, s2s2: 1},
                {s1s1: 0, s1s2: 0, s2s1: 0, s2s2: 0},
                {s1s1: 1, s1s2: 0, s2s1: 1, s2s2: 0},
                {s1s1: 4, s1s2: 0, s2s1: 3, s2s2: 2},
                {s1s1: 0, s1s2: 1, s2s1: 0, s2s2: 1},
                {s1s1: 1, s1s2: 2, s2s1: 2, s2s2: 3},
                {s1s1: 1, s1s2: 0, s2s1: 1, s2s2: 0},
            ],
        },
    },

    {
        parameters: [{label: "Seitenlänge a, e und d:", value: 100}]/*, {label:"Winkel A:", value: 120}]*/,
        angles: [undefined, undefined, undefined, undefined, undefined],
        explorer: 'Rice',
        found: [1975, 1976, 1977],
        id: 13,
        init() {
            let l = this.parameters[0].value;
            this.strokes = [l, UNDEF, UNDEF, l * 2, l];

            this.angles[1] = this.angles[4] = 90;
            this.angles[0] = 120;
            this.angles[3] = 360 - 2 * this.angles[0];
            this.angles[2] = 360 - this.angles[0] - this.angles[3];
        },
        strokes: [],
        primitiveUnit: {
            clockwise: [false, true, true, false, true, false, false, true],
            colors: ['yellow', 'blue', 'blue', 'yellow', 'green', 'purple', 'purple', 'green'],
            connection: [
                [],
                [0, 0],
                [3, 2],
                [0, 0],
                [0, 3],
                [0, 0],
                [3, 2],
                [1, 1],
            ],
            count: 8,
            offset: [
                {childId1: 7, segment1: 2, childId2: 5, segment2: 2},
                {childId1: 5, segment1: 2, childId2: 7, segment2: 2},

                {childId1: 7, segment1: 3, childId2: 0, segment2: 0},
                {childId1: 0, segment1: 0, childId2: 7, segment2: 3},

                {childId1: 7, segment1: 0, childId2: 0, segment2: 3},
                {childId1: 0, segment1: 3, childId2: 7, segment2: 0},
            ],
            rotation: [
                {s1s1: UNDEF, s1s2: UNDEF, s2s1: UNDEF, s2s2: UNDEF},
                {s1s1: 0, s1s2: 1, s2s1: 0, s2s2: 1},
                {s1s1: 2, s1s2: 3, s2s1: 3, s2s2: 2},
                {s1s1: 0, s1s2: 1, s2s1: 0, s2s2: 1},
                {s1s1: 3, s1s2: 2, s2s1: 0, s2s2: 4},
                {s1s1: 0, s1s2: 1, s2s1: 0, s2s2: 1},
                {s1s1: 2, s1s2: 3, s2s1: 3, s2s2: 2},
                {s1s1: 1, s1s2: 0, s2s1: 1, s2s2: 0},
            ],
        },
    },
    {
        parameters: [{label: "Seitenlänge b:", value: 80}],
        angles: [90, 145.34, 69.32, 124.66, 110.68],
        explorer: 'Stein',
        found: 1985,
        id: 14,
        init() {
            let length = this.parameters[0].value;
            this.angles = [90, 145.34, 69.32, 124.66, 110.68];
            this.strokes = [length, length, length, length * 2, length * 2]
            this.strokes[1] *= Math.sqrt((11 * Math.sqrt(57) - 25) / 8)
        },
        primitiveUnit: {
            clockwise: [false, true, false, true, true, true],
            colors: ['blue', 'yellow', 'blue', 'yellow', 'red', 'red'],
            connection: [
                [],
                [1, 2],
                [1, 1],
                [1, 2],
                [4, 2],
                [0, 1],
            ],
            count: 6,
            offset: [
                {childId1: 1, segment1: 3, childId2: 5, segment2: 3},
                {childId1: 5, segment1: 3, childId2: 1, segment2: 3},

                {childId1: 1, segment1: 3, childId2: 3, segment2: 4},
                {childId1: 3, segment1: 4, childId2: 1, segment2: 3},

                {childId1: 5, segment1: 3, childId2: 3, segment2: 4},
                {childId1: 3, segment1: 4, childId2: 5, segment2: 3},
            ],
            rotation: [
                {s1s1: 0, s1s2: 4, s2s1: 0, s2s2: 1},
                {s1s1: 1, s1s2: 2, s2s1: 2, s2s2: 3},
                {s1s1: 1, s1s2: 0, s2s1: 1, s2s2: 0},
                {s1s1: 1, s1s2: 2, s2s1: 2, s2s2: 3},
                {s1s1: 0, s1s2: 4, s2s1: 2, s2s2: 1},
                {s1s1: 0, s1s2: 1, s2s1: 1, s2s2: 0},
            ],
        },
        strokes: [],
    },
    {
        parameters: [{label: "Seitenlänge d:", value: 50}],
        angles: [150, 60, 135, 105, 90],
        explorer: 'Mann-McLoud-Von Derau',
        found: 2015,
        id: 15,
        init() {
            let length = this.parameters[0].value;
            this.strokes = [length, 2 * length, length, length, length]
            this.strokes[3] *= (Math.sqrt(2)) / (Math.sqrt(3) - 1)
            this.angles = [150, 60, 135, 105, 90];
        },
        strokes: [],
        primitiveUnit: {
            clockwise: [true, false, true, true, false, false, false, false, true, true, false, true],
            colors: ['yellow', 'purple', 'red', 'blue', 'orange', 'lightyellow', 'lightyellow', 'orange', 'blue', 'red', 'purple', 'yellow'],
            connection: [
                [],
                [2, 4],
                [2, 2],
                [3, 0],
                [3, 3],
                [4, 2],
                [1, 2],
                [2, 4],
                [2, 2],
                [4, 4],
                [3, 3],
                [4, 2],
            ],
            count: 12,
            offset: [
                {childId1: 10, segment1: 4, childId2: 7, segment2: 4},
                {childId1: 7, segment1: 4, childId2: 10, segment2: 4},

                {childId1: 11, segment1: 1, childId2: 0, segment2: 2},
                {childId1: 0, segment1: 2, childId2: 11, segment2: 1},

                {childId1: 0, segment1: 2, childId2: 11, segment2: 3},
                {childId1: 11, segment1: 3, childId2: 0, segment2: 2},
            ],
            rotation: [
                {s1s1: 4, s1s2: 3, s2s1: 3, s2s2: 2}, //überarbeiten, weil rotation probably nicht "stimmt"
                {s1s1: 2, s1s2: 1, s2s1: 4, s2s2: 3},
                {s1s1: 2, s1s2: 3, s2s1: 2, s2s2: 3},
                {s1s1: 4, s1s2: 3, s2s1: 4, s2s2: 0},
                {s1s1: 3, s1s2: 2, s2s1: 3, s2s2: 2},
                {s1s1: 4, s1s2: 0, s2s1: 2, s2s2: 1},
                {s1s1: 2, s1s2: 1, s2s1: 1, s2s2: 2},
                {s1s1: 2, s1s2: 1, s2s1: 4, s2s2: 0},
                {s1s1: 2, s1s2: 3, s2s1: 2, s2s2: 3},
                {s1s1: 4, s1s2: 3, s2s1: 0, s2s2: 4},
                {s1s1: 3, s1s2: 2, s2s1: 3, s2s2: 2},
                {s1s1: 4, s1s2: 3, s2s1: 2, s2s2: 1},
            ]
        },
    },
];
