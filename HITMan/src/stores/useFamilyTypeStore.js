import {defineStore} from "pinia";

/**
 * Also hier wird für die jetzige Familie alles gespeichert, was wichtig ist. Wenn man die Familie wechselt, müssen alle diese Infos in einen "dauerthaften" Store verschoben werden
 * Der Store braucht jegliche Art von Infos, da diese Dinge auch auf das Auswahlmenü im Header übertragen werden sollen*/


export const useFamilyTypeStore = defineStore('familyType', {
    state: () => ({
        name: 'Reinhardt1',
    }), //hier speicher ich, wie ich jedes Fünfeck definiere, kann man ja vllt auch als ID realisieren
    getters: {
        getName: (state) => state.name
    }, //hier kann man eventuell die winkel und seitenlängen von dem jetzigen Fünfeck zurückgeben, durch computed kann man das ja auch easy berechnen
})
